﻿using System.Linq;
using Android.Net.Wifi.P2p;

namespace Wifip2p.Receiver.Code
{
    public class PeerListListener : Java.Lang.Object, WifiP2pManager.IPeerListListener
    {
        private readonly MainActivity _activity;

        public PeerListListener(MainActivity activity)
        {
            _activity = activity;
        }

        public void OnPeersAvailable(WifiP2pDeviceList peers)
        {
        }
    }
}
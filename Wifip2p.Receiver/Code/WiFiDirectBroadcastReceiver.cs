﻿using Android.App;
using Android.Content;
using Android.Net.Wifi.P2p;
using Android.Text;
using Android.Widget;

namespace Wifip2p.Receiver.Code
{
    public class WiFiDirectBroadcastReceiver : BroadcastReceiver
    {
        private WifiP2pManager mManager;
        private WifiP2pManager.Channel mChannel;
        private MainActivity mActivity;

        public WiFiDirectBroadcastReceiver(WifiP2pManager manager, WifiP2pManager.Channel channel,
            MainActivity activity)
        {
            this.mManager = manager;
            this.mChannel = channel;
            this.mActivity = activity;
        }

        public override void OnReceive(Context context, Intent intent)
        {
            string action = intent.Action;

            if (WifiP2pManager.WifiP2pStateChangedAction.Equals(action))
            {
                // Check to see if Wi-Fi is enabled and notify appropriate activity
                var wifiP2PInfo = (WifiP2pInfo)intent.GetParcelableExtra(WifiP2pManager.ExtraWifiP2pInfo);
                mActivity.SetConnectionInfo(wifiP2PInfo);
            }
            else if (WifiP2pManager.WifiP2pPeersChangedAction.Equals(action))
            {
                // Call WifiP2pManager.requestPeers() to get a list of current peers
                this.mManager.RequestPeers(this.mChannel, new PeerListListener(this.mActivity));
            }
            else if (WifiP2pManager.WifiP2pConnectionChangedAction.Equals(action))
            {
                // Respond to new connection or disconnections
                var wifiP2PInfo = (WifiP2pInfo)intent.GetParcelableExtra(WifiP2pManager.ExtraWifiP2pInfo);
                mActivity.SetConnectionInfo(wifiP2PInfo);
            }
            else if (WifiP2pManager.WifiP2pThisDeviceChangedAction.Equals(action))
            {
                // Respond to this device's wifi state changing
                mActivity.UpdateThisDevice((WifiP2pDevice)intent.GetParcelableExtra(WifiP2pManager.ExtraWifiP2pDevice));
            }
        }
    }
}
using Android.OS;
using Android.Text;

namespace Wifip2p.Receiver
{
    public static class StringExtensions
    {
        public static ISpanned ToAndroidSpanned(this string str)
        {
            ISpanned charSequence;
            if (Build.VERSION.SdkInt >= BuildVersionCodes.N)
            {
                charSequence = Html.FromHtml(str, FromHtmlOptions.ModeLegacy);
            }
            else
            {
#pragma warning disable 618
                charSequence = Html.FromHtml(str);
#pragma warning restore 618
            }

            return charSequence;
        }
    }
}
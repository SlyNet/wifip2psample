using System.Collections.Generic;
using Android.Net.Wifi.P2p;
using Android.Util;

namespace Wifip2p.Receiver.Code
{
    public class ServiceResponseListener : Java.Lang.Object, WifiP2pManager.IDnsSdServiceResponseListener,
        WifiP2pManager.IDnsSdTxtRecordListener
    {
        private readonly MainActivity activity;

        public ServiceResponseListener(MainActivity activity)
        {
            this.activity = activity;
        }

        public void OnDnsSdServiceAvailable(string instanceName, string registrationType, WifiP2pDevice srcDevice)
        {
            Log.Debug(MainActivity.Tag, "OnDnsSdServiceAvailable: " + instanceName);
            if (instanceName == MainActivity.ServiceName)
            {
                activity.ConnectTo(srcDevice);
            }
        }

        public void OnDnsSdTxtRecordAvailable(string fullDomainName, IDictionary<string, string> txtRecordMap, WifiP2pDevice srcDevice)
        {
            Log.Debug(MainActivity.Tag, "OnDnsSdTxtRecordAvailable. fullDomainName: " + fullDomainName);
        }
    }
}
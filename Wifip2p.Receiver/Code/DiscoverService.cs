﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Android.Net.Wifi.P2p;
using Android.Net.Wifi.P2p.Nsd;
using Android.Util;

namespace Wifip2p.Receiver.Code
{
    public class DiscoverService
    {
        private readonly WifiP2pManager wifiManager;
        private readonly WifiP2pManager.Channel channel;
        private readonly WifiP2pServiceRequest wifiP2PServiceRequest;
        private CancellationTokenSource discoverCancellation;

        public DiscoverService(WifiP2pManager wifiManager, WifiP2pManager.Channel channel)
        {
            this.wifiManager = wifiManager;
            this.channel = channel;
            this.wifiP2PServiceRequest = WifiP2pServiceRequest.NewInstance(ServiceType.All);
        }

        public void StartDiscover()
        {
            this.discoverCancellation = new CancellationTokenSource();
            wifiManager.RemoveServiceRequest(this.channel, this.wifiP2PServiceRequest, new P2PActionListener(() =>
            {
                wifiManager.AddServiceRequest(channel, this.wifiP2PServiceRequest,
                    new P2PActionListener(async () =>
                    {
                        while (!this.discoverCancellation.IsCancellationRequested)
                        {
                            Log.Info(MainActivity.Tag, "DiscoverServices on client called");
                            var taskCompletionSource = new TaskCompletionSource<bool>();
                            wifiManager.DiscoverServices(channel, new P2PActionListener(
                                () =>
                                {
                                    Log.Info(MainActivity.Tag, "Success callback from DiscoverServices");
                                    taskCompletionSource.SetResult(true);
                                },
                                () =>
                                {
                                    Log.Error(MainActivity.Tag, "Error callback from DiscoverServices");
                                    taskCompletionSource.SetResult(false);
                                }));
                            await taskCompletionSource.Task;
                            await Task.Delay(TimeSpan.FromSeconds(5));
                        }
                    }));
            }));
        }

        public void StopDiscover()
        {
            this.discoverCancellation?.Cancel();
        }
    }
}
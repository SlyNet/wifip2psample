﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Net.Wifi.P2p;
using Android.Net.Wifi.P2p.Nsd;
using Android.OS;
using Android.Util;
using Android.Widget;
using Java.Net;
using Wifip2p.Receiver.Code;
using Exception = System.Exception;

namespace Wifip2p.Receiver
{
    [Activity(Label = "Wifip2p.Receiver", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        public const string Tag = "p2p";
        public static readonly int CommunicationPort = 21891;
        private CancellationTokenSource listeningCancellation;
        private WifiP2pManager.Channel channel;
        private WifiP2pInfo connectionInfo;

        private IntentFilter intentFilter;
        private bool listening;
        private WifiP2pDevice myInfo;
        private WiFiDirectBroadcastReceiver receiver;
        private bool serverStarted;
        private WifiP2pManager wifiManager;
        private DiscoverService discoverService;
        private PublishService publishService;
        private ServerSocket serverSocket;

        public static string ServiceName => "Date receiver Test";
        public static string CommunicationPortArgName => "listenport";

        private Button DiscoverBtn => FindViewById<Button>(Resource.Id.Discover);

        private Button ResetButton => FindViewById<Button>(Resource.Id.Reset);

        private Button SendHiButton => FindViewById<Button>(Resource.Id.SendHi);
        private Button StartServerButton => FindViewById<Button>(Resource.Id.StartServer);

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);


            SetContentView(Resource.Layout.Main);

            DiscoverBtn.Click += (sender, args) =>
            {
                ClientDiscoveringState();

                this.discoverService.StartDiscover();
            };

            ResetButton.Click += ResetButtonOnClick;
            SendHiButton.Click += SendHi;
            StartServerButton.Click += StartServerButtonOnClick;

            wifiManager = (WifiP2pManager) GetSystemService(WifiP2pService);
            channel = wifiManager.Initialize(this, MainLooper, null);
            receiver = new WiFiDirectBroadcastReceiver(wifiManager, channel, this);

            var serviceResponseListener = new ServiceResponseListener(this);
            wifiManager.SetDnsSdResponseListeners(channel, serviceResponseListener, serviceResponseListener);

            intentFilter = new IntentFilter();
            intentFilter.AddAction(WifiP2pManager.WifiP2pStateChangedAction);
            intentFilter.AddAction(WifiP2pManager.WifiP2pPeersChangedAction);
            intentFilter.AddAction(WifiP2pManager.WifiP2pConnectionChangedAction);
            intentFilter.AddAction(WifiP2pManager.WifiP2pThisDeviceChangedAction);

            this.discoverService = new DiscoverService(this.wifiManager, this.channel);
            this.publishService = new PublishService(this.wifiManager, this.channel);
        }

        private async void ResetButtonOnClick(object o, EventArgs eventArgs)
        {
            listeningCancellation?.Cancel();
            if (!this.serverSocket?.IsClosed ?? false)
            {
                this.serverSocket?.Close();
            }

            StopServer();
            var groupInfo = await new GroupInfoService().GetGroupInfoAsync(wifiManager, channel);
            if (groupInfo.IsGroupOwner)
                wifiManager.RemoveGroup(channel, new P2PActionListener());
        }

        private async void StopServer()
        {
            await this.publishService.StopServer();
            this.DisconnectedState();
        }

        private async void StartServerButtonOnClick(object o, EventArgs eventArgs)
        {
            ServerPublishingState();
            await this.publishService.StartPublish();
            StartListening();
        }

        private void StartListening()
        {
            listening = true;
            listeningCancellation = new CancellationTokenSource();

            Task.Run(async () =>
            {
                try
                {
                    using (this.serverSocket = new ServerSocket(CommunicationPort))
                    {
                        Log.Debug(Tag, "Server: Socket opened");

                        while (true)
                        {
                            if (listeningCancellation.IsCancellationRequested)
                                throw new TaskCanceledException();

                            var client = await serverSocket.AcceptAsync();
                            var inputStream = client.InputStream;

                            using (var reader = new StreamReader(inputStream))
                            {
                                var message = reader.ReadToEnd();
                                RunOnUiThread(() =>
                                {
                                    new AlertDialog.Builder(this)
                                        .SetMessage(message)
                                        .SetTitle("Received from remote")
                                        .Show();
                                });
                            }
                            await Task.Delay(500);
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                finally
                {
                    listening = false;
                }
            }, listeningCancellation.Token);
        }

        protected override void OnResume()
        {
            base.OnResume();
            RegisterReceiver(receiver, intentFilter);
        }

        protected override void OnPause()
        {
            base.OnPause();
            UnregisterReceiver(receiver);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            StopServer();
        }

        private async void SendHi(object sender, EventArgs eventArgs)
        {
            await Task.Run(() =>
            {
                Socket socket = null;
                try
                {
                    socket = new Socket();
                    var inetSocketAddress = new InetSocketAddress(connectionInfo.GroupOwnerAddress, CommunicationPort);

                    socket.Connect(inetSocketAddress, 5000);
                    using (var writer = new StreamWriter(socket.OutputStream))
                    {
                        writer.Write($"{DateTime.Now:o} from {myInfo.DeviceName}");
                    }
                }
                catch (Exception e)
                {
                    Log.Error(Tag, e.Message);
                }
                finally
                {
                    socket?.Close();
                }
            });
        }

        public void UpdateThisDevice(WifiP2pDevice deviceInfo)
        {
            myInfo = deviceInfo;

            var text = FindViewById<TextView>(Resource.Id.MyInfo);
            var formattableString =
                $"Status: <b>{deviceInfo.Status}</b>; <br/> my address is {deviceInfo.DeviceAddress}; <br/> device name: {deviceInfo.DeviceName}; ";
            text.SetText(formattableString.ToAndroidSpanned(), TextView.BufferType.Spannable);

            if (deviceInfo.Status == WifiP2pDeviceState.Available)
            {
                this.DisconnectedState();
            }
        }

        public void SetConnectionInfo(WifiP2pInfo inf)
        {
            if (inf != null && inf.GroupFormed)
            {
                this.connectionInfo = inf;

                var text = FindViewById<TextView>(Resource.Id.ConnectionInfo);
                var info = inf.IsGroupOwner
                    ? "Is <font color='red'>Server</font>"
                    : "Is <font color='yellow'>Client</font>";
                text.SetText(info.ToAndroidSpanned(), TextView.BufferType.Spannable);

                if (!inf.IsGroupOwner)
                    ClientConnectedState();
                else
                    ServerConnectedState();
            }
            else
            {
                var text = FindViewById<TextView>(Resource.Id.ConnectionInfo);
                text.Text = "";

                if (serverStarted)
                    ServerPublishingState();
                else
                    DisconnectedState();
            }
        }

        private void ServerConnectedState()
        {
            StartServerButton.Enabled = false;
            SendHiButton.Enabled = false;
            ResetButton.Enabled = true;
            DiscoverBtn.Enabled = false;

            //this.publishService.StopServer();
        }

        private void ServerPublishingState()
        {
            StartServerButton.Enabled = false;
            SendHiButton.Enabled = false;
            ResetButton.Enabled = false;
            DiscoverBtn.Enabled = false;

            this.discoverService.StopDiscover();
        }

        private void ClientDiscoveringState()
        {
            StartServerButton.Enabled = false;
            SendHiButton.Enabled = false;
            ResetButton.Enabled = false;
            DiscoverBtn.Enabled = true;
        }

        private void ClientConnectedState()
        {
            StartServerButton.Enabled = false;
            SendHiButton.Enabled = true;
            ResetButton.Enabled = false;
            DiscoverBtn.Enabled = false;

            this.publishService.StopServer();
            this.discoverService.StopDiscover();
        }

        private void DisconnectedState()
        {
            StartServerButton.Enabled = true;
            SendHiButton.Enabled = false;
            ResetButton.Enabled = false;
            DiscoverBtn.Enabled = true;

            this.discoverService.StopDiscover();
            this.publishService.StopServer();
        }

        public void ConnectTo(WifiP2pDevice target)
        {
            var wifiP2PConfig = new WifiP2pConfig
            {
                DeviceAddress = target.DeviceAddress
            };

            wifiManager.Connect(channel, wifiP2PConfig, new P2PActionListener());
        }
    }
}
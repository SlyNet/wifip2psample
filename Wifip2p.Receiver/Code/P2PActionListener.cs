﻿using System;
using Android.Net.Wifi.P2p;
using Android.Util;

namespace Wifip2p.Receiver.Code
{
    public class P2PActionListener : Java.Lang.Object, WifiP2pManager.IActionListener
    {
        private readonly Action onSuccess;
        private readonly Action onFailure;

        public P2PActionListener(Action onSuccess = null, Action onFailure = null)
        {
            this.onSuccess = onSuccess;
            this.onFailure = onFailure;
        }

        public void OnFailure(WifiP2pFailureReason reason)
        {
            Log.Error(MainActivity.Tag, "P2PActionListener.OnFailure: " + reason);
            onFailure?.Invoke();
        }

        public void OnSuccess()
        {
            Log.Debug(MainActivity.Tag, "P2PActionListener.OnSuccess");
            onSuccess?.Invoke();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Android.Net.Wifi.P2p;
using Android.Net.Wifi.P2p.Nsd;
using Android.Util;

namespace Wifip2p.Receiver.Code
{
    public class PublishService
    {
        private readonly WifiP2pManager wifiManager;
        private readonly WifiP2pManager.Channel channel;
        private readonly WifiP2pDnsSdServiceInfo serviceInfo;
        private CancellationTokenSource discoveryCancellation;

        public PublishService(WifiP2pManager wifiManager, WifiP2pManager.Channel channel)
        {
            this.wifiManager = wifiManager;
            this.channel = channel;
            serviceInfo = WifiP2pDnsSdServiceInfo.NewInstance(MainActivity.ServiceName, "_presence._tcp",
                new Dictionary<string, string>
                {
                    {MainActivity.CommunicationPortArgName, MainActivity.CommunicationPort.ToString()}
                });
        }

        public Task StartPublish()
        {
            var task = new TaskCompletionSource<bool>();

            this.discoveryCancellation = new CancellationTokenSource();
            wifiManager.ClearLocalServices(this.channel, new P2PActionListener(() =>
            {
                wifiManager.AddLocalService(channel, serviceInfo, new P2PActionListener(() =>
                {
                    this.wifiManager.CreateGroup(this.channel, new P2PActionListener());
                    task.SetResult(true);
                }, () => task.SetResult(false)));
            }));

            return task.Task;
        }

        public Task StopServer()
        {
            var task = new TaskCompletionSource<bool>();
            this.discoveryCancellation?.Cancel();
            Log.Debug(MainActivity.Tag, "StopServer called");

            wifiManager.RemoveLocalService(channel, serviceInfo, 
                new P2PActionListener(() => task.SetResult(true), () => task.SetResult(false)));

            return task.Task;
        }
    }
}
﻿using System.Threading.Tasks;
using Android.Net.Wifi.P2p;

namespace Wifip2p.Receiver.Code
{
    public class GroupInfoService
    {
        public Task<WifiP2pGroup> GetGroupInfoAsync(WifiP2pManager manager, WifiP2pManager.Channel channel)
        {
            TaskCompletionSource<WifiP2pGroup> result = new TaskCompletionSource<WifiP2pGroup>();
            manager.RequestGroupInfo(channel, new GroupInfoListener(result));
            return result.Task;
        }
    }

    internal class GroupInfoListener : Java.Lang.Object, WifiP2pManager.IGroupInfoListener
    {
        private readonly TaskCompletionSource<WifiP2pGroup> _result;

        public GroupInfoListener(TaskCompletionSource<WifiP2pGroup> result)
        {
            _result = result;
        }

        public void OnGroupInfoAvailable(WifiP2pGroup @group)
        {
            _result.SetResult(@group);
        }
    }
}